<?php 

function theme_boxes(){
	$servises = new_cmb2_box([
		'id'			=> 'sericon',
		'title'			=> 'Servises icon',
		'object_types'		=> ['servises'],
	
	]);
	
	
	$servises-> add_field([
		
		'name'		=> 'Icme name',
		'id'		=> 'sic',
		'type'		=> 'text',
		'desc'=> 'Name must be like icon1, icon2 ',
		
	]);
	
	
}

add_action('cmb2_init', 'theme_boxes');	