<?php global $redux_demo; ?>

	<!-- Footer Starts Here ---->
	<div class="footer">
		<div class="container">
			<div class="footer-top">
				 <a href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-bot.png" class="img-responsive" alt=""/></a>
			</div>
			<p class="footer-head"><?php echo $redux_demo['fcr']; ?></p>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- Footer Ends Here ---->
	
	<?php wp_footer(); ?>
</body>
</html>