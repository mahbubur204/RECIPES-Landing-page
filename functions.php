<?php 
	
	function theme_suppots(){
		
		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');
		add_theme_support('menus');
		add_theme_support('widgets');
		
		
		register_nav_menus([
			'main_menu'		=> 'Header Menu',
		]);
			
	}
	
	add_action('after_setup_theme', 'theme_suppots');

	require_once("inc/scripts/scripts.php");
	require_once("inc/customPost/customPost.php");
	
	require_once("cmb/init.php");
	require_once("cmb/config.php");
	
	
	require_once("redux/ReduxCore/framework.php");
	require_once("redux/sample/reduxConfig.php");