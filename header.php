<?php global $redux_demo; ?>

<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->


<!DOCTYPE HTML>
<html>
<head>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<!-- Custom Theme files -->

<!-- Custom Theme files -->
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300' rel='stylesheet' type='text/css'>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> 
addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/move-top.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/easing.js"></script>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
					</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.jscrollpane.min.js"></script>
		<script type="text/javascript" id="sourcecode">
			$(function()
			{
				$('.scroll-pane').jScrollPane();
			});
		</script>
		
		
		
		
	<?php wp_head(); ?>	
		
</head>
<body>
	<!--- Header Starts Here --->
	<div class="header" id="home">
		<div class="container">
			<div class="logo">
				 <a href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt=""></a>
			</div>
			<div class="menu">
				<ul class="menu-top">
					<li><a class="play-icon popup-with-zoom-anim" href="#small-dialog">Log In</a></li>
					<li><a class="play-icon popup-with-zoom-anim" href="#small-dialog1">Sign up</a></li>
					<li><div class="main">
								<section>
									<button id="showRight" class="navig"></button>
								</section>
						</div></li>
				</ul>	
				<!---pop-up-box---->
					  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.min.js"></script>    
					<link href="<?php echo get_template_directory_uri(); ?>/css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
					<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.js" type="text/javascript"></script>
					<!---//pop-up-box---->
				<div id="small-dialog" class="mfp-hide">
						<div class="login">
							<h3>Log In</h3>
							<h4>Already a Member</h4>
							<input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" />
							<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}"/>
							<input type="submit" value="Login" />
						</div>
					</div>
					<div id="small-dialog1" class="mfp-hide">
						<div class="signup">
							<h3>Sign Up</h3>
							<h4>Enter Your Details Here</h4>
							<input type="text" value="First Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'First Name';}" />
							<input type="text" value="Second Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Second Name';}" />
							<input type="text" class="email"value="Enter Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Email';}"  />
							<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}"/>
							<input type="submit"  value="SignUp"/>
						</div>
					</div>	
				 <script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>															
				<!--- Navigation from Right To Left --->
				<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/component.css" />
					<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>
					<script type="text/javascript">
			
					  var _gaq = _gaq || [];
					  _gaq.push(['_setAccount', 'UA-7243260-2']);
					  _gaq.push(['_trackPageview']);
			
					  (function() {
					    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					  })();
			
					</script>
					<div class="cbp-spmenu-push">
						<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
							<h3>Menu</h3>
							
							<?php wp_nav_menu([
								'theme_lication'	=> 'main_menu',
								'menu_class'		=> ' ',
								'menu_id'			=> ' ',
								
							]); ?>
							
							
							
							
							
							
						</nav>
				</div>
				<script src="<?php echo get_template_directory_uri(); ?>/js/classie.js"></script>
					<script>
						var menuRight = document.getElementById( 'cbp-spmenu-s2' ),
							showRight = document.getElementById( 'showRight' ),
							body = document.body;
			
						showRight.onclick = function() {
							classie.toggle( this, 'active' );
							classie.toggle( menuRight, 'cbp-spmenu-open' );
							disableOther( 'showRight' );
						};
			
						function disableOther( button ) {
							if( button !== 'showRight' ) {
								classie.toggle( showRight, 'disabled' );
							}
						}
					</script>
				<!--- Navigation from Right To Left --->
					<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/easing.js"></script>
								
				</div> 
				<div class="clearfix"></div>
				<div class="header-bottom">
					<p><?php echo $redux_demo['tone']; ?></p>
					<h1><?php echo $redux_demo['htext']; ?></h1>
					
					<?php if($redux_demo['btn']): ?>
					<a href="<?php echo $redux_demo['blink']; ?>"><?php echo $redux_demo['btn']; ?></a>
					<?php endif; ?>
					<p class="reward"><?php echo $redux_demo['hbtext']; ?></p>
				</div>
		</div>
	</div>