<?php

	function registerPost(){
		
		register_post_type('servises',[
				'public'		=> true,
				'labels'		=> [
					'name'		=> 'Servises',
					'add_new'	=> 'Add New Servise',
					'all_items'	=> 'All Servise',
				],
				'supports'            => [ 'title', 'editor', 'thumbnail',  ],
		
		]);
		
		register_post_type('team',[
				'public'		=> true,
				'labels'		=> [
					'name'		=> 'Team Members',
					'add_new'	=> 'Add New Member',
					'all_items'	=> 'All Members',
				],
				'supports'            => [ 'title', 'editor', 'thumbnail'],
				'menu_icon'				=>'dashicons-businessman',	
		
		]);
		
		register_post_type('gellary',[
				'public'		=> true,
				'labels'		=> [
					'name'		=> 'gellaries',
					'add_new'	=> 'Add New gellary',
					'all_items'	=> 'All gellaries',
				],
				'supports'            => [ 'title', 'editor', 'thumbnail'],
				'menu_icon'				=>'dashicons-businessman',	
		
		]);
		
		
	}
	add_action('init','registerPost');