<?php get_header(); ?>
	<!--- Header Ends Here --->
	<!-- Aboutus Starts Here --->
	<div class="aboutus" id="about">
		<div class="container">
			<div class="row aboutus-row">
			
				<?php 
					$serv =	new WP_Query([
							'post_type'	 		=> 'servises',
							'posts_per_page'	=> 3,
						]); 
					
					while($serv->have_posts()) : $serv->the_post();	
						
				?>
				<div class="col-md-4 aboutus-row-column">
				
	
					<i class="<?php echo get_post_meta(get_the_ID(), 'sic', true); ?>"></i>
					<h3><?php the_title(); ?></h3>
					<span class="line-red"></span>
					<?php the_content(); ?>
				</div>
				
			<?php endwhile; ?>		
				
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- Aboutus Ends Here --->
	<!-- Interduce Starts Here  ---->
	<div class="interduce">
		<div class="container">
			<div class="row interduce-row">
				<div class="col-md-6 inter-row-column2">
					<img src="<?php echo get_template_directory_uri(); ?>/images/iphone-1.png" alt=""/>
				</div>
				<div class="col-md-6 inter-row-column">
					<h4><span>INTRODUCING</span> THE FUDI APP</h4>
					<p>Morbi eget posuere dolor. Pellentesque cursus aliquet aliquet. Aeneanet felis sit amet diam mollis ullamcorper. Nullam consequat sem a ante vest ibulum tristique. Suspendisse tristique lacus ac mattis porta. </p>
					<p>Vivamus ligula quam, vehicula non lacinia sed, faucibus sit amet libero. In libero dui, eleifend eu nisi id, egestas fringilla odio. In varius quam a massa hendrerit ullamcorper a eu justo. Suspendisse porta mattis convallis.Aenean tempus ligula ac odio rhoncus, quis aliquam dolor accumsan. </p>
					<p>Suspendisse aliquet felis consectetur libero congue, sed pulvinar diam malesuada. Duis vehicula a nibh id hendrerit. Donec sit amet ultricesante, a mattis massa. </p>
					<ul class="inter-duce">
						<li><i class="icon4"></i></li>
						<li><i class="icon5"></i></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- Interduce Ends Here  ---->
	<!--- Slider Starts Here --->
	<div class="slider">
		<div class="container">
			<div class="slider-content">
				<script src="<?php echo get_template_directory_uri(); ?>/js/responsiveslides.min.js"></script>
			 <script>
			    // You can also use "$(window).load(function() {"
			    $(function () {
			      // Slideshow 4
			      $("#slider4").responsiveSlides({
			        auto: true,
			        pager: true,
			        nav: true,
			        speed: 500,
			        namespace: "callbacks",
			        before: function () {
			          $('.events').append("<li>before event fired.</li>");
			        },
			        after: function () {
			          $('.events').append("<li>after event fired.</li>");
			        }
			      });
			
			    });
			  </script>
			<!----//End-slider-script---->
			<!-- Slideshow 4 -->
			
			
			
			    <div  id="top" class="callbacks_container">
			      <ul class="rslides" id="slider4">
				  
				  <?php 
				
							$team = new WP_Query([
								
								'post_type'		=> 'team',
								'posts_per_page'	=> 3,
							
							]);
						
						while($team->have_posts()) : $team->the_post(); ?>
			        <li>
			          <div class="slider-top">
			          	<?php the_post_thumbnail(); ?>
			          	<?php the_content(); ?>
			          	<p class="below"><?php the_title(); ?></p>
			          </div>
			        </li>
				<?php endwhile; ?>	
			       
			      </ul>
			    </div>
			    <div class="clearfix"> </div>
	  			<!--- banner Slider Ends Here ---> 
			</div>
			</div>
			</div>
		</div>
	</div>
	<!--- Slider Ends Here --->
	<!--- Portfolio Starts Here --->
	<div class="portfolio" id="portfolio">
		<div class="container">
			<div class="portfolio-top">
				<h3>BROWSE BY CUISINES</h3>
				<span class="linet-red"></span>
			</div>
		</div >
		<div class="portfolio-start">
		
		<?php 
			$gellary = new WP_Query([
			
				'post_type'		=> 'gellary',
				'posts_per_page'	=> 10,
				
			]);
		while($gellary ->have_posts()) : $gellary->the_post(); ?>
				<div class="portfolio-img">
					<a href="" class="play-icon popup-with-zoom-anim"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" alt=""/></a>
					<div id="small-dialog-it" class="mfp-hide">
						<div class="portfolio-items">
						<?php the_post_thumbnail(); ?>
						<h4><?php the_title(); ?></h4>
						<?php the_content(); ?>
						</div>
					</div>
				</div>
			<?php endwhile; ?>	
				
				
				<div class="clearfix"></div>
		</div>
	</div>
		<div class="container">
			<ul class="numbers">
				<li>
					<div class="number-top">
						<h4>23,567</h4>
						<p>Recipes Available</p>
					</div>
				</li>
				<li>
					<div class="number-top">
						<h4>431,729</h4>
						<p>Active Users</p>
					</div>
				</li>
				<li>
					<div class="number-top">
						<h4>892,173</h4>
						<p>Positive Reviews</p>
					</div>
				</li>
				<li>
					<div class="number-top">
						<h4>56,581</h4>
						<p>Photos & Videos</p>
					</div>
				</li>
				<li>
					<div class="number-top">
						<h4>3,182</h4>
						<p>Spices and Herbs</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!--- Portfolio Ends Here --->
<?php get_footer(); ?>
	
